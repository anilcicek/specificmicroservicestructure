package com.patch.discovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Configuration;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

@Configuration
@EnableAutoConfiguration
@EnableAdminServer
@EnableEurekaServer
@SpringBootApplication
public class ServiceRegistryApplication {

    public static void main(String[] args) {
    	
		try {
			long sleepTime = 10L * 1000L;
			Thread.sleep(sleepTime);	
			System.out.println("DiscoveryApplication wait...");
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
        SpringApplication.run(ServiceRegistryApplication.class, args);
    }

}