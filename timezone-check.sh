#!/bin/bash

#container names
container_auth=onclm-auth
container_gateway=onclm-gateway
container_core=onclm-core
container_discovery=onclm-discovery
container_config=onclm-config

echo
echo "--"
echo "--Current Date on host"
echo "--"
echo "`date`"
echo 

echo "--"
echo "--Current Date on containers"
echo "--"
echo "Date on $container_auth      : `docker exec -it  $container_auth date`"
echo "Date on $container_gateway   : `docker exec -it  $container_gateway date`"
echo "Date on $container_core      : `docker exec -it  $container_core date`"
echo "Date on $container_discovery : `docker exec -it  $container_discovery date`"
echo "Date on $container_config    : `docker exec -it  $container_config date`"
