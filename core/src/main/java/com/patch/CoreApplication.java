package com.patch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.WebApplicationInitializer;

import com.patch.repository.UserRepository;

@EnableEurekaClient
@ComponentScan
@SpringBootApplication
public class CoreApplication extends SpringBootServletInitializer implements WebApplicationInitializer {
	
	@Autowired UserRepository userRepository;

	public static void main(String[] args) {
		
		SpringApplication.run(CoreApplication.class, args);
	}
 
}
