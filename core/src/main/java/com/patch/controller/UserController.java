package com.patch.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.patch.model.User;
import com.patch.repository.UserRepository;

@RestController
@RequestMapping("/users")
public class UserController {
	
	@Autowired UserRepository userRepository;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<User> getAllUsers() {
	  return userRepository.findAll();
	}

}
