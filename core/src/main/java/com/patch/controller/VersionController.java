package com.patch.controller;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.patch.common.VersionType;


@RestController
@RequestMapping("/version")
public class VersionController {
	
	@RequestMapping(value = "/papi/getVersion", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity <?> getVersion() throws IOException{
		     InputStream inputStream= null;
		     String result="";
		     VersionType version = new VersionType();
		try {
			Properties prop = new Properties();
			String propFileName = "VERSION.txt";
			
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			 
			if (inputStream != null) {
			prop.load(inputStream);
			} else {
			throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
			 
			 
			// get the property value and print it out
			String date = prop.getProperty("DATE");
			String buildId = prop.getProperty("BUILD_ID");
			String gitCommitId = prop.getProperty("GIT_COMMIT_ID");
			
			
			version.setDate(date);
			version.setGitCommitId(gitCommitId);
			version.setBuildId(buildId);
			
			
			} catch (Exception e) {
			System.out.println("Exception: " + e);
			} finally {
			inputStream.close();
			}


		
		
	        return  new ResponseEntity<>(version,HttpStatus.OK);
	    }

}
