package  com.patch.common;

/*
* This class is used by Business class.
* It includes Every enumeration in the Onclm-Core,
* like serviceReturnType,QuestionType..
* 
* @author olcayyalgin
* @version 1.0.0
* @since  2018-10-01
*/

public final class GeneralEnumerations {
	
	public GeneralEnumerations() {
		
	}
	
	
	public enum ExternalRestServiceReturnTypes{
	    ERROR(9999),
	    SUCCESS(100);
		
	    private int returnCode;

	    private ExternalRestServiceReturnTypes(int returnCode) {
	      this.returnCode = returnCode;
	    }

	    public int getReturnCode() {
	      return returnCode;
	    }
	}
	
	

}
