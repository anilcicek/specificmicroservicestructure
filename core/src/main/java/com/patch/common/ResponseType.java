package com.patch.common;


/*
* This class is used web service Return type.
* 
* 
* @author olcayyalgin
* @version 1.0.0
* @since  2018-11-12
*/

public class ResponseType {
	
	int returnCode;
	String returnMessage;
	
	
	public ResponseType() {
		
	}
	
	public ResponseType(int returnCode,String returnMessage) {
		this.returnCode=returnCode;
		this.returnMessage=returnMessage;
	}
	
	public int getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}
	public String getReturnMessage() {
		return returnMessage;
	}
	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

}
