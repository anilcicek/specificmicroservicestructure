package com.patch.common;

 /*
 * This class is used as a type which is shown on Approval Screen 
 * 
 * @author olcayyalgin
 * @version 1.0.0
 * @since  2016-11-12
 */
public class LabelValue {
	 protected String label;
	 protected String value;
	 
	public LabelValue(String label, String value) {
		super();
		this.label = label;
		this.value = value;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
