package com.patch.common;

/*
* This class is used by ClaimContoller as WebService type.
* 
* @author olcayyalgin
* @version 1.0.0
* @since  2018-10-01
*/

public class KeyValue {
	 protected String key;
	 protected String value;
	 
	public KeyValue() {
		 
		 
	} 
	
	public KeyValue(String key, String value) {
		super();
		this.key = key;
		this.value = value;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}


}
