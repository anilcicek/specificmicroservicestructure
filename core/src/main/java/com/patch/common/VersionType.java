package com.patch.common;

public class VersionType {
	
	private String date;
	private String gitCommitId;
	private String buildId;
	
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getGitCommitId() {
		return gitCommitId;
	}
	public void setGitCommitId(String gitCommitId) {
		this.gitCommitId = gitCommitId;
	}
	public String getBuildId() {
		return buildId;
	}
	public void setBuildId(String buildId) {
		this.buildId = buildId;
	}

}
