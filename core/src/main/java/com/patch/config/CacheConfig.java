package com.patch.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;


@Configuration
public class CacheConfig {
	
	protected final Logger logger = LoggerFactory.getLogger(CacheConfig.class);
	
	@CacheEvict(allEntries = true, value = { "params" })
	@Scheduled(fixedDelay = 10 * 60 * 1000, initialDelay = 1000)
	public void invalidateCachesToEnforceRefresh() {
		logger.info("Invalidating cache to enforce it to be refreshed.");
	}

}
